import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ICredentials, IToken } from './login.model';
import {map, tap} from 'rxjs/operators';
import { ICredentialsDto, ITokenDto } from '../common/resource/login/login.dto';
import { LoginResource } from '../common/resource/login/login.resource';
import {MeService} from '../common/me/me.service';

@Injectable()
export class LoginService {
    constructor(
        private loginResource: LoginResource,
        private meService: MeService
    ) {
    }

    login(credentials: ICredentials): Observable<IToken> {
        const credentialsDto = this.modelToDto(credentials);
        return this.loginResource.login(credentialsDto)
            .pipe(
                tap(tokenDto => this.meService.login(tokenDto)),
                map(dto => this.dtoToModel(dto))
            );
    }

    private dtoToModel(dto: ITokenDto): IToken {
        return {
            token: dto.token
        };
    }

    private modelToDto(model: ICredentials): ICredentialsDto {
        return {
            login: model.login,
            pass: model.password
        };
    }

}
