import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { AuthTokenService } from '../me/auth-token.service';
import { catchError } from 'rxjs/operators';
import {MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition} from '@angular/material/snack-bar';

enum  ErrorType {
    resourceIdFormat = 'RESOURCE_ID_FORMAT',
    resourceIdNotFound = 'RESOURCE_ID_NOT_FOUND',
    resourceTypeNotFound = 'RESOURCE_TYPE_NOT_FOUND',
    unhandledError = 'UNHANDLED_ERROR',
    invalidCredentials = 'INVALID_CREDENTIALS',
    missingToken = 'MISSING_TOKEN',
    invalidToken = 'INVALID_TOKEN',
    missingRole = 'MISSING_ROLE'
}

@Injectable()
export class ErrorHandlerInterceptor implements HttpInterceptor {
    horizontalPosition: MatSnackBarHorizontalPosition = 'center';
    verticalPosition: MatSnackBarVerticalPosition = 'top';

    constructor(private snackbarService: MatSnackBar) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req)
            .pipe(
                catchError(error => {
                    // console.error(error);
                    const message = this.getMessage(error.error?.type);
                    const panelClass = 'error-snack';
                    this.openSnackBar(message, panelClass);
                    return throwError(error);
                })
            );
    }

    private getMessage(errorType: ErrorType): string {
        switch (errorType) {
            case ErrorType.missingRole:
                return 'Missing roles';
            case ErrorType.invalidCredentials:
                return 'Invalid credentials';
            case ErrorType.invalidToken:
                return 'Invalid token';
            case ErrorType.missingToken:
                return 'Missing token';
            case ErrorType.resourceIdFormat:
                return 'Resource id format';
            case ErrorType.resourceIdNotFound:
                return 'Resource id not found';
            case ErrorType.resourceTypeNotFound:
                return 'Resource type not found';
            case ErrorType.unhandledError:
                return 'Unhandled error';
            default:
                return 'Unhandled error';
        }
    }

    openSnackBar(message: string, panelClassName: string): void {
        this.snackbarService.open(message, 'Close', {
            duration: 5000,
            panelClass: panelClassName,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
        });
    }
}
