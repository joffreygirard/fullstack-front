import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IMovie } from '../../../movie/movie.model';
import { IMovieDto, IMovieIdDto } from './movie.dto';

@Injectable()
export class MovieResource {

    private url = 'http://localhost:3000/movies';

    constructor(
        private http: HttpClient
    ) {
    }

    findAll(): Observable<IMovieIdDto[]> {
        return this.http.get<IMovieIdDto[]>(this.url);
    }

    create(movie: IMovieDto): Observable<IMovieDto> {
        return this.http.post<IMovieDto>(this.url, movie);
    }

    delete(id: string): Observable<number> {
        const deleteUrl = this.url + '/' + id;
        return this.http.delete<number>(deleteUrl);
    }
}
