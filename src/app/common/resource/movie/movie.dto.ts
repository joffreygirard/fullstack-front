export interface IMovieDto {
    title: string;
    year: number;
    imageUrl: string;
}

export interface IMovieIdDto {
    id: string;
    title: string;
    year: number;
    imageUrl: string;
}
