import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ICharacter } from '../../../character/character.model';
import {ICharacterDto, ICharacterIdDto} from './character.dto';

@Injectable()
export class CharacterResource {

    private url = 'http://localhost:3000/characters';

    constructor(
        private http: HttpClient
    ) {
    }

    findAll(): Observable<ICharacterIdDto[]> {
        return this.http.get<ICharacterIdDto[]>(this.url);
    }

    create(character: ICharacterDto): Observable<ICharacterDto> {
        return this.http.post<ICharacterDto>(this.url, character);
    }

    delete(id: string): Observable<number> {
        const deleteUrl = this.url + '/' + id;
        return this.http.delete<number>(deleteUrl);
    }
}
