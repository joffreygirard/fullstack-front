export interface ICharacterDto {
    firstName: string;
    lastName: string;
    birthDate: string;
}

export interface ICharacterIdDto {
    id: string;
    firstName: string;
    lastName: string;
    birthDate: string;
}
