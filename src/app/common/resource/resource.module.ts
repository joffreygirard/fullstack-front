import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CharacterResource } from './character/character.resource';
import { MovieResource } from './movie/movie.resource';
import { AuthTokenService } from '../me/auth-token.service';
import { AuthenticationInterceptor } from '../interceptor/authentication.interceptor';
import { ErrorHandlerInterceptor } from '../interceptor/error-handler.interceptor';
import { LoginResource } from './login/login.resource';

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule
    ],
    providers: [
        CharacterResource,
        MovieResource,
        LoginResource,
        AuthTokenService,
        { provide: HTTP_INTERCEPTORS, useClass: AuthenticationInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorHandlerInterceptor, multi: true }
    ]
})
export class ResourceModule { }
