export interface ICredentialsDto {
    login: string;
    pass: string;
}

export interface ITokenDto {
    token: string;
}

export enum UserRole {
    member = 'MEMBER',
    admin = 'ADMIN'
}

export interface IMeDto {
    id: string;
    username: string;
    role: string;
}
