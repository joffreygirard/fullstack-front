import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatChipsModule } from '@angular/material/chips';
import { MatListModule } from '@angular/material/list';
import { CharacterItemComponent } from './character-item/character-item.component';
import { CharacterListComponent } from './character-list/character-list.component';
import { CharacterComponent } from './character.component';
import { MatCardModule } from '@angular/material/card';
import { CharacterEditComponent } from './character-edit/character-edit.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CharacterRoutingModule } from './character-routing.module';

@NgModule({
  declarations: [
    CharacterComponent,
    CharacterItemComponent,
    CharacterListComponent,
    CharacterEditComponent
  ],
    imports: [
        CommonModule,
        CharacterRoutingModule,
        MatListModule,
        MatChipsModule,
        MatCardModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        MatButtonModule,
        RouterModule,
        MatGridListModule,
        MatSnackBarModule
    ],
  exports: [
    CharacterComponent
  ]
})
export class CharacterModule { }

