import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CharacterService } from '../character.service';
import { ICharacter } from '../character.model';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';

@Component({
    selector: 'app-character-edit',
    templateUrl: './character-edit.component.html',
    styleUrls: ['./character-edit.component.scss']
})
export class CharacterEditComponent {
    formGroup = new FormGroup({
        firstName: new FormControl(undefined, Validators.required),
        lastName: new FormControl(undefined, Validators.required),
        birthDate: new FormControl(undefined, Validators.required),
    });
    horizontalPosition: MatSnackBarHorizontalPosition = 'center';
    verticalPosition: MatSnackBarVerticalPosition = 'top';

    constructor(private characterService: CharacterService, private snackBar: MatSnackBar) {
        this.reset();
    }

    save(): void {
        if (this.formGroup.valid) {
            const character: ICharacter = {
                firstName: this.formGroup.value.firstName,
                lastName: this.formGroup.value.lastName,
                birthDate: this.formGroup.value.birthDate,
            };
            this.characterService.saveItem(character)
                .subscribe(createdCharacter => {
                    console.log('Created character : ' + createdCharacter);
                    this.reset();
                    const message = 'Character successfully created !';
                    const panelClass = 'success-snack';
                    this.openSnackBar(message, panelClass);
                }, error => {});
        }
    }

    reset(): void {
        this.formGroup.reset({
            birthDate: '1970-01-01'
        });
    }

    openSnackBar(message: string, panelClassName: string): void {
        this.snackBar.open(message, 'Close', {
            duration: 5000,
            panelClass: panelClassName,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
        });
    }

}
