import { Component, Input } from '@angular/core';
import { ICharacterId } from '../character.model';
import { CharacterService } from '../character.service';

@Component({
    selector: 'app-character-list',
    templateUrl: './character-list.component.html',
    styleUrls: ['./character-list.component.scss']
})
export class CharacterListComponent {

    @Input() items: ICharacterId[] | undefined | null;

    constructor(private characterService: CharacterService) {
    }

    refreshItemsList(): void {
        this.characterService.getItems()
            .subscribe(characters => {
                this.items = characters;
            });
    }
}
