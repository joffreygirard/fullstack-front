export interface ICharacter {
    firstName: string;
    lastName: string;
    birthDate: string;
}

export interface ICharacterId {
    id: string;
    firstName: string;
    lastName: string;
    age: number;
}

export enum CharacterItemAttribute {
    firstName = 'FIRST_NAME',
    lastName = 'LAST_NAME',
    birthDate = 'BIRTH_YEAR'
}

