import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CharacterComponent } from './character.component';
import { CharacterEditComponent } from './character-edit/character-edit.component';
import { MeGuard } from '../common/me/me.guard';

const routes: Routes = [
    {
        path: '',
        component: CharacterComponent
    },
    {
        canActivate: [MeGuard],
        path: 'edit',
        component: CharacterEditComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CharacterRoutingModule { }

