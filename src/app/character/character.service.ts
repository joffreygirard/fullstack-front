import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CharacterResource } from '../common/resource/character/character.resource';
import { ICharacter, ICharacterId } from './character.model';
import { ICharacterDto, ICharacterIdDto } from '../common/resource/character/character.dto';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

@Injectable()
export class CharacterService {
    constructor(private characterResource: CharacterResource) {
    }

    getItems(): Observable<ICharacterId[]> {
        return this.characterResource.findAll()
            .pipe(
                map((dtos: ICharacterIdDto[]) => {
                    return dtos.map(this.dtoToModelId);
                })
            );
    }

    saveItem(character: ICharacter): Observable<ICharacter> {
        const characterDto = this.modelToDto(character);
        return this.characterResource.create(characterDto)
            .pipe(
                map(dto => this.dtoToModel(dto))
            );
    }

    deleteItem(id: string): Observable<number> {
        return this.characterResource.delete(id);
    }

    private dtoToModel(dto: ICharacterDto): ICharacter {
        return {
            firstName: dto.firstName,
            lastName: dto.lastName,
            birthDate: dto.birthDate
        };
    }

    private dtoToModelId(dto: ICharacterIdDto): ICharacterId {
        return {
            id: dto.id,
            firstName: dto.firstName,
            lastName: dto.lastName,
            age: moment().diff(moment(dto.birthDate), 'years')
        };
    }

    private modelToDto(model: ICharacter): ICharacterDto {
        return {
            firstName: model.firstName,
            lastName: model.lastName,
            birthDate: model.birthDate
        };
    }

}
