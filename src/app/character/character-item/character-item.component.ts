/* tslint:disable:no-trailing-whitespace */
import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { ICharacterId } from '../character.model';
import { CharacterService} from '../character.service';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';

@Component({
    selector: 'app-character-item',
    templateUrl: './character-item.component.html',
    styleUrls: ['./character-item.component.scss']
})
export class CharacterItemComponent implements OnInit, OnChanges, OnDestroy {
    horizontalPosition: MatSnackBarHorizontalPosition = 'center';
    verticalPosition: MatSnackBarVerticalPosition = 'top';

    @Input() item: ICharacterId | undefined;
    @Output() refreshItemsList = new EventEmitter();

    constructor(private characterService: CharacterService, private snackBar: MatSnackBar) {
    }
    ngOnInit(): void {
    }
    ngOnChanges(changes: SimpleChanges): void {
    }
    ngOnDestroy(): void {
    }

    delete(id: string|null): void {
        if (id) {
            this.characterService.deleteItem(id)
                .subscribe(response => {
                    console.log('item deleted : ' + id);
                    this.refreshItemsList.emit();
                    const message = 'Character successfully deleted !';
                    const panelClass = 'success-snack';
                    this.openSnackBar(message, panelClass);
                }, error => {
                    this.refreshItemsList.emit();
                });
        }
    }

    openSnackBar(message: string, panelClassName: string): void {
        this.snackBar.open(message, 'Close', {
            duration: 5000,
            panelClass: panelClassName,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
        });
    }
}
