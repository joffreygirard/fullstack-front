import { Component } from '@angular/core';
import { IMeDto } from './common/resource/login/login.dto';
import { MeService } from './common/me/me.service';
import { Observable } from 'rxjs';

interface IMenuItem {
    label: string;
    link: string;
}

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'Front';
    me$: Observable<IMeDto | null> = this.meService.getMe();

    constructor(private meService: MeService) {
    }

    menuItems: IMenuItem[] = [
        {
            label: 'Personnages',
            link: 'characters'
        },
        {
            label: 'Films',
            link: 'movies'
        },
        {
            label: 'Login',
            link: 'login'
        },
        {
            label: 'Admin',
            link: 'admin'
        }
    ];

    logout(): void {
        this.meService.logout();
    }
}
