import { Component, Input } from '@angular/core';
import { IMovieId } from '../movie.model';
import { MovieService } from '../movie.service';

@Component({
    selector: 'app-movie-list',
    templateUrl: './movie-list.component.html',
    styleUrls: ['./movie-list.component.scss']
})
export class MovieListComponent {
    @Input() items: IMovieId[] | undefined;

    constructor(private movieService: MovieService) {
    }

    refreshItemsList(): void {
        this.movieService.getItems()
            .subscribe(movies => {
                this.items = movies;
            });
    }
}
