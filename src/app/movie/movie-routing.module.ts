import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MovieComponent } from './movie.component';
import { MovieEditComponent } from './movie-edit/movie-edit.component';
import { MeGuard } from '../common/me/me.guard';

const routes: Routes = [
    {
        path: '',
        component: MovieComponent
    },
    {
        canActivate: [MeGuard],
        path: 'edit',
        component: MovieEditComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MovieRoutingModule { }

