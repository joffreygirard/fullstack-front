import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MovieComponent } from './movie.component';
import { MovieListComponent } from './movie-list/movie-list.component';
import { MovieItemComponent } from './movie-item/movie-item.component';
import { MovieEditComponent } from './movie-edit/movie-edit.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MovieRoutingModule } from './movie-routing.module';

@NgModule({
    declarations: [
        MovieComponent,
        MovieListComponent,
        MovieItemComponent,
        MovieEditComponent
    ],
    imports: [
        CommonModule,
        MovieRoutingModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        FormsModule,
        ReactiveFormsModule,
        MatIconModule,
        RouterModule,
        MatButtonModule,
        MatChipsModule
    ],
    exports: [
        MovieComponent
    ]
})
export class MovieModule { }

