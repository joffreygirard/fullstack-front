import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MovieService } from '../movie.service';
import { IMovie } from '../movie.model';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';

@Component({
    selector: 'app-movie-edit',
    templateUrl: './movie-edit.component.html',
    styleUrls: ['./movie-edit.component.scss']
})
export class MovieEditComponent {
    formGroup = new FormGroup({
        title: new FormControl(undefined, Validators.required),
        imageUrl: new FormControl(undefined, Validators.required),
        year: new FormControl(undefined, Validators.required),
    });
    horizontalPosition: MatSnackBarHorizontalPosition = 'center';
    verticalPosition: MatSnackBarVerticalPosition = 'top';

    constructor(private movieService: MovieService, private snackBar: MatSnackBar) {
        this.reset();
    }

    save(): void {
        if (this.formGroup.valid) {
            const movie: IMovie = {
                title: this.formGroup.value.title,
                year: this.formGroup.value.year,
                imageUrl: this.formGroup.value.imageUrl,
            };
            this.movieService.saveItem(movie)
                .subscribe(createdMovie => {
                    console.log('Created movie : ' + createdMovie);
                    this.reset();
                    const message = 'Movie successfully created !';
                    const panelClass = 'success-snack';
                    this.openSnackBar(message, panelClass);
                }, error => {});
        }
    }

    reset(): void {
        this.formGroup.reset({
            year: 2000
        });
    }

    openSnackBar(message: string, panelClassName: string): void {
        this.snackBar.open(message, 'Close', {
            duration: 5000,
            panelClass: panelClassName,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
        });
    }

}
