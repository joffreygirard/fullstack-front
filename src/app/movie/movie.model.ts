export interface IMovie {
    title: string;
    year: number;
    imageUrl: string;
}

export interface IMovieId {
    id: string;
    title: string;
    year: number;
    imageUrl: string;
}

export enum CharacterItemAttribute {
    title = 'TITLE',
    year = 'YEAR',
    imageUrl = 'IMAGE_URL'
}
