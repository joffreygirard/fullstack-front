import { Component } from '@angular/core';
import { MovieService } from './movie.service';
import { IMovieId } from './movie.model';

@Component({
    selector: 'app-movie',
    templateUrl: './movie.component.html',
    styleUrls: ['./movie.component.scss'],
    providers: [
        MovieService
    ]
})
export class MovieComponent {
    errorCharacter = false;
    items: IMovieId[] | undefined;

    constructor(private service: MovieService) {
        this.initComponent();
    }

    initComponent(): void {
        this.service.getItems().subscribe(movies => {
            this.items = movies;
        }, error => {
            console.log(error);
            this.errorCharacter = true;
        }, () => {
            console.log('complete');
            this.errorCharacter = false;
        });
    }
}

