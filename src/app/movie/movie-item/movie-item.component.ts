/* tslint:disable:no-trailing-whitespace */
import {Component, EventEmitter, Input, Output} from '@angular/core';
import { IMovieId } from '../movie.model';
import { MovieService } from '../movie.service';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';

@Component({
    selector: 'app-movie-item',
    templateUrl: './movie-item.component.html',
    styleUrls: ['./movie-item.component.scss']
})
export class MovieItemComponent {
    horizontalPosition: MatSnackBarHorizontalPosition = 'center';
    verticalPosition: MatSnackBarVerticalPosition = 'top';

    @Input() item: IMovieId | undefined;
    @Output() refreshItemsList = new EventEmitter();

    constructor(private movieService: MovieService, private snackBar: MatSnackBar) {
    }

    delete(id: string|null): void {
        if (id) {
            this.movieService.deleteItem(id)
                .subscribe(response => {
                    console.log('item deleted : ' + id);
                    this.refreshItemsList.emit();
                    const message = 'Movie successfully deleted !';
                    const panelClass = 'success-snack';
                    this.openSnackBar(message, panelClass);
                }, error => {
                    this.refreshItemsList.emit();
                });
        }
    }

    openSnackBar(message: string, panelClassName: string): void {
        this.snackBar.open(message, 'Close', {
            duration: 5000,
            panelClass: panelClassName,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
        });
    }
}
