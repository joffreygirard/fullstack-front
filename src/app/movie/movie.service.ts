import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MovieResource } from '../common/resource/movie/movie.resource';
import { IMovie, IMovieId } from './movie.model';
import { IMovieDto, IMovieIdDto } from '../common/resource/movie/movie.dto';
import { map } from 'rxjs/operators';

@Injectable()
export class MovieService {

    constructor(private movieResource: MovieResource) {
    }

    getItems(): Observable<IMovieId[]> {
        return this.movieResource.findAll()
            .pipe(
                map((dtos: IMovieIdDto[]) => {
                    return dtos.map(dto => {
                        return {
                            id: dto.id,
                            title: dto.title,
                            year: dto.year,
                            imageUrl: dto.imageUrl
                        };
                    });
                })
            );
    }

    saveItem(movie: IMovie): Observable<IMovie> {
        const movieDto = this.modelToDto(movie);
        return this.movieResource.create(movieDto)
            .pipe(
                map(dto => this.dtoToModel(dto))
            );
    }

    deleteItem(id: string): Observable<number> {
        return this.movieResource.delete(id);
    }

    private dtoToModel(dto: IMovieDto): IMovie {
        return {
            title: dto.title,
            year: dto.year,
            imageUrl: dto.imageUrl
        };
    }

    private modelToDto(model: IMovie): IMovieDto {
        return {
            title: model.title,
            year: model.year,
            imageUrl: model.imageUrl
        };
    }
}
