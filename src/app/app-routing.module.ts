import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {
    path: 'characters',
    loadChildren: () => import('./character/character.module')
        .then(module => module.CharacterModule)
  },
  {
    path: 'movies',
    loadChildren: () => import('./movie/movie.module')
        .then(module => module.MovieModule)
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'admin',
    loadChildren: () => import('./admin/admin.module')
        .then(module => module.AdminModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
