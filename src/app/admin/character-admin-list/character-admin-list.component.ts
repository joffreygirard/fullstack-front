import { Component } from '@angular/core';
import { ICharacterId } from '../../character/character.model';
import { CharacterService } from '../../character/character.service';

const ELEMENT_DATA: ICharacterId[] = [
  {id: 'test', firstName: 'Harry', lastName: 'Potter', age: 20},
];

@Component({
  selector: 'app-character-admin-list',
  templateUrl: './character-admin-list.component.html',
  styleUrls: ['./character-admin-list.component.scss']
})
export class CharacterAdminListComponent {
  displayedColumns: string[] = ['id', 'firstName', 'lastName', 'age', 'action'];
  items$ = this.service.getItems();

  constructor(private service: CharacterService) { }


}
