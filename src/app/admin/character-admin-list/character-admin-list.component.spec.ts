import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CharacterAdminListComponent } from './character-admin-list.component';

describe('CharacterAdminListComponent', () => {
  let component: CharacterAdminListComponent;
  let fixture: ComponentFixture<CharacterAdminListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CharacterAdminListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterAdminListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
