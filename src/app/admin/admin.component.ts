import { Component } from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent {
  managedElements = [
    {
      name: 'Characters', route: 'characters'
    },
    {
      name: 'Movies', route: 'movies'
    }
  ];

  constructor() { }


}
