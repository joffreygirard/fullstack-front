import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MovieAdminListComponent } from './movie-admin-list.component';

describe('MovieAdminListComponent', () => {
  let component: MovieAdminListComponent;
  let fixture: ComponentFixture<MovieAdminListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MovieAdminListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieAdminListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
