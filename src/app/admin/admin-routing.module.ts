import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MeGuard } from '../common/me/me.guard';
import { AdminComponent } from './admin.component';
import { CharacterAdminListComponent } from './character-admin-list/character-admin-list.component';
import { MovieAdminListComponent } from './movie-admin-list/movie-admin-list.component';

const routes: Routes = [
    {
        path: '',
        component: AdminComponent
    },
    {
        // canActivate: [MeGuard],
        path: 'characters',
        component: CharacterAdminListComponent
    },
    {
        // canActivate: [MeGuard],
        path: 'movies',
        component: MovieAdminListComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminRoutingModule { }

