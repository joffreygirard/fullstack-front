import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { AdminRoutingModule } from './admin-routing.module';
import { MatChipsModule } from '@angular/material/chips';
import { CharacterAdminListComponent } from './character-admin-list/character-admin-list.component';
import { MovieAdminListComponent } from './movie-admin-list/movie-admin-list.component';
import { MatTableModule } from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';



@NgModule({
    declarations: [
        AdminComponent,
        CharacterAdminListComponent,
        MovieAdminListComponent
    ],
    imports: [
        CommonModule,
        AdminRoutingModule,
        MatChipsModule,
        MatTableModule,
        MatIconModule
    ],
    exports: [
        AdminComponent
    ]
})
export class AdminModule { }
